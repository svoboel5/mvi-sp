# Wide and Deep NN pro rekomendační systémy
Zadání semestrální práce jsem dostala od firmy Recombee. Cílem je otestovat použitelnost Wide and Deep neuronové sítě pro rekomendační systémy. Vstupem algoritmu jsou interakce uživatele s jednotlivými položkami a výstupem je seznam položek, které by se mohly uživateli líbit. Úkolem je porovnat několik možných implementací wide and deep modelu a porovnat je jak s baseline rekomendačními modely - top N populárních položek (které uživatel ještě neviděl), tak s jednotlivými částmi sítě - samostatným wide modelem a samostatným deep modelem.

Data spadají pod NDA.

## Popis repozitáře:
* report.pdf - Popis semestrální práce
* kviz.png - výsledek deep learning kvizu
* Evaluation_colab.ipynb - trénink a měření finálních modelů (v prostřední Google Colab)
* DataPreparation.ipynb - Notebook připravující data pro zpracování modely
* Notes.pdf - Moje poznámky k načteným článků (podrobnější), tutoriálům a dalším zdrojům které jsem použila při práci